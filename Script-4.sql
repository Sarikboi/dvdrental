SELECT
    a.actor_id,
    a.first_name,
    a.last_name,
    CURRENT_DATE - MAX(f.release_year) AS days_since_last_role
FROM
    actor a
LEFT JOIN
    film_actor fa ON a.actor_id = fa.actor_id
LEFT JOIN
    film f ON fa.film_id = f.film_id
GROUP BY
    a.actor_id, a.first_name, a.last_name
ORDER BY
    days_since_last_role DESC;
