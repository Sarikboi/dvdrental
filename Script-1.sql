WITH StaffRevenue AS (
    SELECT
        s.store_id,
        s.staff_id,
        s.first_name,
        s.last_name,
        SUM(p.amount) AS total_revenue
    FROM
        staff s
    INNER JOIN
        payment p ON s.staff_id = p.staff_id
    WHERE
        EXTRACT(YEAR FROM p.payment_date) = 2017
    GROUP BY
        s.store_id, s.staff_id, s.first_name, s.last_name
),
RankedStaffRevenue AS (
    SELECT
        sr.*,
        ROW_NUMBER() OVER (PARTITION BY sr.store_id ORDER BY sr.total_revenue DESC) AS revenue_rank
    FROM
        StaffRevenue sr
)
SELECT
    rsr.store_id,
    rsr.staff_id,
    rsr.first_name,
    rsr.last_name,
    rsr.total_revenue
FROM
    RankedStaffRevenue rsr
WHERE
    rsr.revenue_rank = 1;
