SELECT
    subq.film_id,
    subq.movie_title,
    subq.movie_rating,
    subq.rental_count
FROM (
    SELECT
        f.film_id,
        f.title AS movie_title,
        f.rating AS movie_rating,
        COUNT(r.rental_id) AS rental_count
    FROM
        film f
    JOIN
        inventory i ON f.film_id = i.film_id
    JOIN
        rental r ON i.inventory_id = r.inventory_id
    GROUP BY
        f.film_id, f.title, f.rating
    ORDER BY
        rental_count DESC
    LIMIT 5
) AS subq;
