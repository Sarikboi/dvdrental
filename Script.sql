SELECT
    s1.store_id,
    s1.staff_id,
    s1.first_name,
    s1.last_name,
    SUM(p.amount) AS total_revenue
FROM
    staff s1
INNER JOIN
    payment p ON s1.staff_id = p.staff_id
WHERE
    EXTRACT(YEAR FROM p.payment_date) = 2017
    AND (
        SELECT COUNT(*)
        FROM staff s2
        WHERE s2.store_id = s1.store_id
        AND (
            SELECT SUM(amount)
            FROM payment p2
            WHERE p2.staff_id = s2.staff_id
            AND EXTRACT(YEAR FROM p2.payment_date) = 2017
        ) > (
            SELECT SUM(amount)
            FROM payment p3
            WHERE p3.staff_id = s1.staff_id
            AND EXTRACT(YEAR FROM p3.payment_date) = 2017
        )
    ) = 0
GROUP BY
    s1.store_id, s1.staff_id, s1.first_name, s1.last_name;
