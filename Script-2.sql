SELECT
    f.film_id,
    f.title AS movie_title,
    f.rating AS movie_rating,
    COUNT(r.rental_id) AS rental_count
FROM
    film f
INNER JOIN
    inventory i ON f.film_id = i.film_id
INNER JOIN
    rental r ON i.inventory_id = r.inventory_id
GROUP BY
    f.film_id, f.title, f.rating
ORDER BY
    rental_count DESC
LIMIT 5;
